# tr.gnu

# set print "-"

# set path of config snippets
set loadpath '.'
# load config snippets
load 'dark2.pal'

# set terminal x11 enhanced nopersist noraise font "DejaVu Serif, 30, , medium"
set terminal x11 enhanced nopersist noraise
set ytics nomirror
set xrange [0:*]
# set xlabel "Time (in seconds)" font ", 30"
# set ylabel "Initiators: response time (in seconds)" font ", 30"
set xlabel "Time (in seconds)"
set ylabel "Initiators: response time (in seconds)"
set yrange [-0.1:tacc*1.3]
set arrow from 0,tacc to graph 1, first tacc nohead lw 3
# tr(x) = (2 - 1 / (sigma * tp)) * x
plot for [i=1:initsize] 'log/tr'.i.'.log' using 1:2 with lines ls i lw 2 \
			title 'Initiator'.i
			# tr(x) with lines lw 2 lc black title "tr(t)"

pause 1
reread
