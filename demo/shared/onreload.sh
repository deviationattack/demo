#!/usr/bin/env bash

exec &> /dev/null

$sudo apparmor_parser -R /etc/apparmor.d/usr.sbin.tcpdump || true

if [ "$software" = "strongswan" ]; then
    $sudo apparmor_parser -R \
        /etc/apparmor.d/usr.lib.ipsec.charon || true
    $sudo apparmor_parser -R \
        /etc/apparmor.d/usr.lib.ipsec.stroke || true
fi
