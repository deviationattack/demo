if [ ! -d "/vagrant" ]; then
    shared="../shared"
else
    shared="/vagrantshared"
fi

source "$shared/prepre.sh"
realopt=${name}real
real=${!realopt:-false}

if [ ! -d "/vagrant" ] && [ "$real" != true ] ; then
    exit
fi

if [ ! -d "/vagrant" ]; then
    sudo="sudo "
    ifaceopt=${name}iface
    iface=${!ifaceopt:-eth0}
else
    sudo=""
    iface="enp0s8"
fi

manualtmp="/tmp/manual"
log="/vagrant/log/$name.log"
stdout="/vagrant/log/$name.out"
stderr="/vagrant/log/$name.err"
pcap="/vagrant/log/$name.pcap"
[ "$software" == "strongswan" ] && daemon="charon" \
    || daemon="pluto"

appendonce() {
    local string="$1"
    local file="$2"
    if [ -f "$file" ] && ! grep "^$string$" "$file"; then
        echo "$string" >> "$file"
    fi
}

countpackets() {
    local count matchstr
    count=0
    matchstr='000000002120220800000000'
    count=$(ngrep -I $pcap -xX $matchstr dst 192.168.33.20 \
        2> /dev/null \
        | grep "U 192.168.33.3" 2> /dev/null \
        | wc -l) || true
    echo $count
    return 0
}

# # Gets start date from $log and puts it in variable startdate.
# getstartdate() {
    # startdate=$(grep -A 1 "Starting at:" $runlog | \
        # tail -n 1)
    # starttimesec=$(date -u -d "$starttime" +"%s")
# }

source "$shared/showlogs.sh"

# This is preferable because provisioning causes a broken pipe
# and sends SIGPIPE to the process if there is something on
# stdout.
if [[ "$sourcedebug" != true ]]; then
    if [[ "$rundebug" = true ]]; then
        exec > >(tee >(cat >&5)) 2>&1
    else
        exec > >(tee -a $stdout | cat >&5)
        exec 2> >(tee -a $stderr | cat >&5)
    fi
fi
