#!/usr/bin/env bash

echo "Stopping at:" >> $log
date >> $log

source $shared/killsoftware.sh
source $shared/killprobe.sh
if [ -f "$manualtmp" ]; then
    rm $manualtmp
fi
