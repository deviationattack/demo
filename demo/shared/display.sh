#!/usr/bin/env bash

# $shared must be set appropriately before sourcing this script

kill=false
while [[ "$1" =~ ^- && ! "$1" == "--" ]]; do
    case $1 in
        -k | --kill )
            kill=true
            ;;
    esac
    shift
done
if [[ "$1" == '--' ]]; then
    shift
fi

if [ "$kill" = true ]; then
    pkill gnuplot
    exit 0
fi

source $shared/prepre.sh
