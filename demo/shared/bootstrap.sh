#!/usr/bin/env bash

#exec &> /dev/null

export DEBIAN_FRONTEND=noninteractive
# chmod -R 777 /var/cache/apt/archives

locale-gen en_US.UTF-8
$sudo apt-get update
$sudo apt-get install -y git bc

if [ -f $shared/dotfiles.sh ]; then
    source $shared/dotfiles.sh
fi

appendonce "sudo su -" /home/vagrant/.bashrc
appendonce "sudo su -" /home/vagrant/.zshrc
appendonce "cd /vagrant" /root/.zshrc
appendonce 'source /vagrantshared/{demo,run}conf.sh' /root/.zshrc
appendonce "cd /vagrant" /root/.bashrc
appendonce 'source /vagrantshared/{demo,run}conf.sh' /root/.bashrc

configure Storage persistent /etc/systemd/journald.conf
