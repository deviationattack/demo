#!/usr/bin/env bash

# exec &> /dev/null

# This is needed for Libreswan installation to not stop and
# ask us things
export DEBIAN_FRONTEND=noninteractive

$sudo apt-get autoremove -y --purge $software

exit 0
