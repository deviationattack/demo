# success.gnu

# set terminal x11 enhanced nopersist noraise font "DejaVu Serif, 30, , medium"
set terminal x11 enhanced nopersist noraise
set style line 1 lw 2
# set xlabel "Time (in seconds)" font ", 30"
# set ylabel "Connection success indicator" font ", 30"
set xlabel "Time (in seconds)"
set ylabel "Connection success indicator"
set xrange [0:*]
set yrange [-0.1:1.5]
set ytics 0,1
plot "log/success.log" using 1:2 with lines ls 1 notitle

pause 1
reread
