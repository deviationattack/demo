# tr.gnu

# set print "-"

# set terminal x11 enhanced nopersist noraise font "DejaVu Serif, 30, , medium"
set terminal x11 enhanced nopersist noraise
set style line 1 lw 2
set style line 2 lw 2 lc black
set ytics nomirror
set xrange [0:*]
# set xlabel "Time (in seconds)" font ", 30"
# set ylabel "Probe: response time (in seconds)" font ", 30"
set xlabel "Time (in seconds)"
set ylabel "Probe: response time (in seconds)"
set yrange [-0.1:tacc*1.3]
set arrow from 0,tacc to graph 1, first tacc nohead lw 3
# This curve is translated by 10 compared to the Initiators' curve because the
# attack starts ~10 seconds later than we start Probe.
# tr(x) = (2 - 1 / (sigma * tp)) * (x - 10)
plot "log/tr.log" using 1:2 with lines ls 1 notitle
	 # tr(x) with lines ls 2 title "tr(t)"

pause 1
reread
