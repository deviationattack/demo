# display.gnu

# set terminal x11 enhanced nopersist noraise font "DejaVu Serif, 30, , medium"
set terminal x11 enhanced nopersist noraise
set style line 1 lw 2
set ytics nomirror
set xrange [0:*]
# set xlabel "Time (in seconds)" font ", 30"
# set ylabel "Memory used by IKEv2 in Victim (in MB)" font ", 30"
set xlabel "Time (in seconds)"
set ylabel "Memory used by IKEv2 in Victim (in MB)"
set yrange [0:capacity*1.3]
set arrow from 0,capacity to graph 1, first capacity nohead lc rgb 'red' lw 3
mem(x) = load + (sigma + sigmaprobe) * m * (x - 17)

plot "log/mem.log" using 1:2 with lines ls 1 notitle, \
		 mem(x) with lines lw 2 lc black title "Theoretical value"

pause 1
reread
