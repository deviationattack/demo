# display.gnu

# set terminal x11 enhanced nopersist noraise font "DejaVu Serif, 30, , medium"
set terminal x11 enhanced nopersist noraise
set style line 1 lw 2
set ytics nomirror
set xrange [0:*]
# set xlabel "Time (in seconds)" font ", 30"
# set ylabel "Measured sigma (in init requests per second)" font ", 30"
set xlabel "Time (in seconds)"
set ylabel "Measured sigma (in init requests per second)"
set yrange [0:sigma*2]
set arrow from 0,sigma to graph 1, first sigma nohead lw 3
plot "log/deviationrate.log" using 1:2 with lines ls 1 notitle
pause 1
reread
